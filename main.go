package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Article struct {
	Id      string `json:"Id"`
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
	Nama    string `json:"nama"`
	Alamat  string `json:"alamat"`
	Telpon  string `json:"telpon"`
	Fax     string `json:"fax"`
}

var Articles []Article

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func returnAllArticles(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllArticles")
	json.NewEncoder(w).Encode(Articles)
}

func returnSingleArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	for _, article := range Articles {
		if article.Id == key {
			json.NewEncoder(w).Encode(article)
		}
	}
}

func createNewArticle(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var article Article
	json.Unmarshal(reqBody, &article)
	Articles = append(Articles, article)
	json.NewEncoder(w).Encode(article)
}

func deleteArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	for index, article := range Articles {
		if article.Id == id {
			Articles = append(Articles[:index], Articles[index+1:]...)
		}
	}

}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/articles", returnAllArticles)
	myRouter.HandleFunc("/article", createNewArticle).Methods("POST")
	myRouter.HandleFunc("/article/{id}", deleteArticle).Methods("DELETE")
	myRouter.HandleFunc("/article/{id}", returnSingleArticle)
	log.Fatal(http.ListenAndServe(":1000", myRouter))
}
func main() {
	Articles = []Article{
		Article{Id: "1", Title: "Hello", Desc: "Article Descrption", Content: "article Content", Nama: "PT. BANK RAKYAT INDONESIA (PERSERO)", Alamat: "Jl. Jend. Sudirman Kav. 44-46, Jakarta 10210", Telpon: "(021) 2510244, 2510254, 2510269-264", Fax: "Fax: (021) 2500077, 2500065"},
		Article{Id: "2", Title: "Hello 2", Desc: "Article Descrption", Content: "article Content", Nama: "PT. BANK MANDIRI (PERSERO)", Alamat: "Plaza Mandiri, Jl. Gatot Subroto Kav. 36-38, Jakarta 12190", Telpon: "(021) 5245006, 5245858, 5245849, 52997777", Fax: "Fax: (021) 5263459, 5263460, 5268246, 52997735"},
	}
	handleRequests()
}
